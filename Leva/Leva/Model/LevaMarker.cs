﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Maps;

namespace Leva
    {
    public class LevaMarker : XObjectNotifier
        {

        string          _name,
                        _imageFile;
        string          _address;
        double          _latitude,
                        _longitude;
        string          _description;
        bool            _isPink         = false;


        public LevaMarker () : base()
            {

            }

        public LevaMarker ( string name, string imageFile ) : base()
            {
            _name = name;
            _imageFile = imageFile;
            }

        public LevaMarker ( string name, string imageFile, double latitude, double longitude ) : base()
            {
            _name = name;
            _imageFile = imageFile;
            _latitude = latitude;
            _longitude = longitude;
            }

        public string MapPinText
            {
            get { return "• " + _address + "\n• " + _description; }
            }

        public string Name
            {
            get { return _name; }
            set
                {
                if ( value == _name )
                    return;

                _name = value;
                NotifyPropertyChanged(() => Name);
                NotifyPropertyChanged(() => MapPinText);
                }
            }

        public string Address
            {
            get { return _address; }
            set
                {
                if ( value == _address )
                    return;

                _address = value;
                NotifyPropertyChanged(() => Address);
                NotifyPropertyChanged(() => MapPinText);
                }
            }


        public string Description
            {
            get { return _description; }
            set
                {
                if ( value == _description )
                    return;

                _description = value;
                NotifyPropertyChanged(() => Description);
                NotifyPropertyChanged(() => MapPinText);
                }
            }

        public string ImageFile
            {
            get { return string.IsNullOrEmpty(_imageFile) ? "" : Device.OS == TargetPlatform.WinPhone ? "Assets/" + _imageFile : _imageFile; }
            set
                {
                if ( value == _imageFile )
                    return;

                _imageFile = value;
                NotifyPropertyChanged(() => ImageFile);
                }
            }

        public double Latitude
            {
            get { return _latitude; }
            set
                {
                if ( value == _latitude )
                    return;

                _latitude = value;
                NotifyPropertyChanged(() => Latitude);
                NotifyPropertyChanged(() => PlacePosition);
                }
            }

        public double Longitude
            {
            get { return _longitude; }
            set
                {
                if ( value == _longitude )
                    return;

                _longitude = value;
                NotifyPropertyChanged(() => Longitude);
                NotifyPropertyChanged(() => PlacePosition);
                }
            }

        Position PlacePosition
            {
            get { return new Position(_latitude, _longitude); }
            }

        Pin CreatePlacePin ()
            {
            return new Pin() { Address = _address, Label = _name, Position = PlacePosition, Type = PinType.Place };
            }

        public LevaPin CreateCustomPin ( string iconAssetFile )
            {
            return new LevaPin(this, this.CreatePlacePin(), iconAssetFile);
            }

        public bool IsPink
            {
            get { return _isPink; }
            set
                {
                if ( value == _isPink )
                    return;

                _isPink = value;
                NotifyPropertyChanged(() => IsPink);
                }
            }
        }


    public class LevaMarkerList : List<LevaMarker>
        {

        static LevaMarkerList      _instance;

        public static LevaMarkerList Instance
            {
            get
                {
                if ( _instance == null )
                    _instance = new LevaMarkerList();

                return _instance;
                }
            }

        protected LevaMarkerList ()
            {
            AddMarkerData();
            }

        private void AddMarkerData ()
            {
            LevaMarker[]   places  =
            {
                new LevaMarker( "Tour Eiffel", "pin.png",   48.858234, 2.293774 )
                    {
                        Address     = "Champs de Mars, Paris",
                        Description = "324 mètres de hauteur (avec antennes)",
                        IsPink      = false,
                    },
                new LevaMarker( "Concorde",    "pin.png",     48.865475, 2.321142 )
                    {
                        Address     = "Rive droite, Paris 75008",
                        Description = "La plus grande place de Paris.",
                        IsPink      = true,
                    },
                new LevaMarker( "Étoile",      "pin.png",       48.873880, 2.295101 )
                    {
                        Address     = "Paris 8, 16 et 17",
                        Description = "La place Charles-de-Gaulle,\nanciennement place de l’Étoile.",
                        IsPink      = false,
                    },
                new LevaMarker( "La Défense",  "pin.png",    48.892418, 2.236180 )
                    {
                        Address     = "Quartier de La Défense",
                        Description = "La Grande Arche de la Fraternité.\nInaugurée en 1989 ",
                        IsPink      = true,
                    },
            };

            foreach ( LevaMarker place in places )
                this.Add(place);
            }
        }
    }
