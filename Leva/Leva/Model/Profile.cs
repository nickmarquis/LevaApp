﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Leva
    {
    public class Profile
        {
        public string Id { get; set; }

        public string Name { get; set; }

        public string UserToken { get; set; }

        public string Email { get; set; }

        public Uri ProfilePicturePath { get; set; }
        }
    }