﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Maps;
using System.Linq.Expressions;
using System.ComponentModel;

namespace Leva
    {
    public class LevaMap : Xamarin.Forms.Maps.Map
        {

        LevaMarkerView     markerView     = new LevaMarkerView();

        public LevaMap () : base()
            {
            }

        public LevaMap ( MapSpan mapSpan ) : base(mapSpan)
            {
            }

        public LevaMarkerView PinInfoPanel
            {
            get { return markerView; }
            }

        public string MapTileTemplate
            {
            get;
            set;
            }

        public void AddCustomPin ( LevaPin customPin )
            {
            if ( customPin == null )
                return;

            base.Pins.Add(new Pin() { BindingContext = customPin, Label = customPin.MapPinLabel, Position = customPin.MapPinPosition, Type = customPin.MapPinType });
            }

        protected override void OnBindingContextChanged ()
            {
            base.OnBindingContextChanged();
            }

        public LevaPin GetPinAtPosition ( Position pos )
            {
            if ( Pins == null || pos == null )
                return null;

            Pin pin = Pins.FirstOrDefault(p => p.Position.Latitude == pos.Latitude && p.Position.Longitude == pos.Longitude);

            return pin == null ? null : pin.BindingContext as LevaPin;
            }

        public Task StartLevantClick ( LevaPin pin )
            {
            var leventPage = new LeventPage ((pin.DataObject as LevaMarker).Name);
            return Navigation.PushAsync( leventPage );
            }
        }

    }

