﻿using System;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Leva
    {
    public class Setup
        {
        public static async Task Init ()
            {
            if ( AccountService.Instance.ReadyToSignIn )
                {
                //await AccountService.Instance.Login(); //log user data.. refresh friends and fun
                }
            if ( !LocationService.Instance.GeoLocator.IsListening )
                await LocationService.Instance.StartLocationService();

            MarkerService.Instance.UpdateAndRetrieveMarkerList();
            await LocationService.Instance.GetLocationAsync();
            }
        }
    }