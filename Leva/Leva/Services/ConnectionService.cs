﻿using System;
using System.Threading.Tasks;

namespace Leva
    {
    public class ConnectivityService
        {
        public static async Task<bool> IsConnected ()
            {
            return await Plugin.Connectivity.CrossConnectivity.Current.IsRemoteReachable(Keys.ApplicationMobileService, 80, 5000);
            }
        }
    }