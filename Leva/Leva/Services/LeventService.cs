﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Leva
{
    public class LeventService
        {
        private static LeventService instance;

        public static LeventService Instance
            {
            get
                {
                if ( instance == null )
                    {
                    instance = new LeventService();
                    }

                return instance;
                }
            }

        public async Task<List<LeventMedia> GetLeventMedias ()
            {
            //using ( var handler = new ZumoAuthHeaderHandler() )
            //    {
            //    using ( var client = MobileServiceClientFactory.CreateClient( handler ) )
            //        {
            //        var friendships = client.GetTable <Moment> ();
            //        var moments = await friendships.CreateQuery ().Where (moment => moment.RecipientUserId == AccountService.Instance.Account.UserId).Select (moment => moment).ToListAsync ();

            //        return moments.OrderByDescending( moment => moment.TimeSent ).ToList();
            //        }
            //    }
            }

        public async Task<bool> SendMoment ( Stream image, List<User> recipients, int displayTime )
            {
            bool result;

            try
                {
                var blob = await SaveMoment (image);
                var imageUrl = blob.Uri.ToString ();

                await SendImageToUsers( imageUrl, recipients, displayTime );

                result = true;
                }
            catch
                {
                result = false;
                }

            return result;
            }

        public async Task DestroyMoment ( Moment moment )
            {
            using ( var handler = new ZumoAuthHeaderHandler() )
                {
                using ( var client = MobileServiceClientFactory.CreateClient( handler ) )
                    {
                    await client.GetTable<Moment>().DeleteAsync( moment );
                    }
                }
            }

        private static async Task<CloudBlockBlob> SaveMoment ( Stream image )
            {
            var blobName = string.Format ("{0}{1}", DateTime.Now.ToString (), Guid.NewGuid ().ToString ()).ToLower ();

            var sas = await FetchSas ();
            var credentials = new StorageCredentials (sas);

            var container = new CloudBlobContainer (new Uri (Keys.ContainerURL), credentials);
            var blob = container.GetBlockBlobReference (blobName);
            await blob.UploadFromStreamAsync( image );

            Xamarin.Insights.Track( "ImageUploaded" );

            return blob;
            }

        private static async Task<string> FetchSas ()
            {
            using ( var handler = new ZumoAuthHeaderHandler() )
                {
                using ( var client = MobileServiceClientFactory.CreateClient( handler ) )
                    {
                    var dictionary = new Dictionary<string, string> ();
                    dictionary.Add( "containerName", Keys.ContainerName );

                    return await client.InvokeApiAsync<string>( "sas", System.Net.Http.HttpMethod.Get, dictionary );
                    }
                }
            }

        private async Task SendImageToUsers ( string imageUrl, List<User> recipients, int displayTime )
            {
            using ( var handler = new ZumoAuthHeaderHandler() )
                {
                using ( var client = MobileServiceClientFactory.CreateClient( handler ) )
                    {
                    var senderUserId = AccountService.Instance.User.Id;
                    var senderProfileImage = AccountService.Instance.User.ProfileImage;
                    var senderName = AccountService.Instance.User.Name;
                    var timeSent = DateTime.UtcNow;

                    foreach ( var user in recipients )
                        {
                        var recipientUserId = user.Id;

                        var moment = new Moment
                            {
                            MomentUrl = imageUrl,
                            SenderUserId = senderUserId,
                            SenderName = senderName,
                            SenderProfileImage = senderProfileImage,
                            RecipientUserId = recipientUserId,
                            DisplayTime = displayTime,
                            TimeSent = timeSent
                            };

                        Xamarin.Insights.Track( "MomentShared" );

                        await client.GetTable<Moment>().InsertAsync( moment );
                        }
                    }
                }
            }
        }
    }
