﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Plugin.Settings;
using Xamarin.Auth;
using Xamarin.Forms;


// Source: http://thirteendaysaweek.com/2013/12/13/xamarin-ios-and-authentication-in-windows-azure-mobile-services-part-iii-custom-authentication/
namespace Leva
    {
    public class AccountService
        {
        private static AccountService   s_instance;
        private Profile                 m_profile = null;

        public static AccountService Instance
            {
            get
                {
                if ( s_instance == null )
                    {
                    s_instance = new AccountService();
                    }

                return s_instance;
                }
            }

        public Profile UserProfil
            {
            get
                {
                return m_profile;
                }
            set
                {
                m_profile = value;
                }
            }

        public string AuthenticationToken { get; set; }
        ///public Account Account { get; set; }
        //public User User { get; set; }

        public bool ReadyToSignIn
            {
            get { return !string.IsNullOrEmpty(AuthenticationToken); }
            }

        private AccountService ()
            {
            FetchAuthenticationToken();
            }

        void FetchAuthenticationToken ()
            {
            var expiration = CrossSettings.Current.GetValueOrDefault<DateTime> ("tokenExpiration");
            if ( expiration != null && DateTime.Compare(expiration, DateTime.Now) > 0 )
                {
                AuthenticationToken = CrossSettings.Current.GetValueOrDefault<string>("authenticationKey");
                }
            }

        public void SaveUser(Profile profile)
            {
            UserProfil = profile;

            CrossSettings.Current.AddOrUpdateValue<string>("profileImage", profile.ProfilePicturePath.ToString());
            CrossSettings.Current.AddOrUpdateValue<string>("profileName", profile.Name);
            CrossSettings.Current.AddOrUpdateValue<string>( "profileEmail", profile.Email );
            SaveAuthenticationToken(profile.UserToken);

            var moreInformation = new Dictionary <string, string> {
                    { "Name", profile.Name },
                    { "Email", profile.Email }
                };

            Xamarin.Insights.Identify(profile.Id, moreInformation);
            }

        public void RetrieveFacebookProfile ()
            {
            DependencyService.Get<IAccountService>().RetrieveFacebookProfile();
            }

        public void SaveAuthenticationToken ( string token )
            {
            AccountService.Instance.AuthenticationToken = token;
            CrossSettings.Current.AddOrUpdateValue<string>( "authenticationKey", token );
            CrossSettings.Current.AddOrUpdateValue<DateTime>( "tokenExpiration", DateTime.Now.AddDays( 30 ) );
            }

        public void SignOut ()
            {
            AccountService.Instance.AuthenticationToken = "";
            CrossSettings.Current.Remove("authenticationKey");
            CrossSettings.Current.Remove("tokenExpiration");
            }
        }

    public interface IAccountService
        {
        void RetrieveFacebookProfile ();
        }
    }