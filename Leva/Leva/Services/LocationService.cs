﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Plugin.Geolocator;
using Plugin.Geolocator.Abstractions;

namespace Leva
    {
    class LocationService
        {
        private static  LocationService         s_instance;
        private IGeolocator                     m_locator;
        private Position                        m_position;
        private double                          m_accuracy;
        private int                             m_refreshTime;

        public LocationService (double accuracy)
            {
            m_accuracy = 20;
            m_refreshTime = 500000;
            m_locator = CrossGeolocator.Current;
            m_locator.DesiredAccuracy = accuracy;
            //m_locator.AllowsBackgroundUpdates = false;
            }

        public static LocationService Instance
            {
            get
                {
                if ( s_instance == null )
                    s_instance = new LocationService (50);

                return s_instance;
                }
            }

        public async Task<bool> StartLocationService ()
            {
            if ( await m_locator.StartListeningAsync(m_refreshTime, m_accuracy, true) )
                {
                m_locator.PositionChanged += locator_PositionChanged;
                return true;
                }
            return false;
            }

        private void locator_PositionChanged ( object sender, PositionEventArgs e )
            {
            m_position = e.Position;
            }

        public async Task<Position> GetLocationAsync ()
            {
            m_position = await m_locator.GetPositionAsync(10000);
            if ( m_position != null )
                return m_position;
            else
                return new Position();
            }

        public async Task<bool> StopLocationService ()
            {
            if ( await m_locator.StopListeningAsync() )
                {
                m_locator.PositionChanged -= locator_PositionChanged;
                return true;
                }
            return false;
            }

        public IGeolocator GeoLocator
            {
            get
                {
                return m_locator;
                }
            set
                {
                m_locator = value;
                }
            }

        public bool IsPositionAvailable ()
            {
            return m_locator.IsGeolocationAvailable && m_position != null;
            }



        public Position GetFastPosition
            {
            get
                {
                if (m_position == null)
                    {
                    return new Position();
                    }
                else
                    {
                    return m_position;
                    }
                }
            }

        public double Accuracy
            {
            get
                {
                return m_accuracy;
                }
            set
                {
                if ( value > 0 )
                    m_accuracy = value;
                }
            }

        public int RefreshTime
            {
            get
                {
                return m_refreshTime;
                }
            set
                {
                if ( value > 0 )
                    m_refreshTime = value;
                }
            }
        }
    }
