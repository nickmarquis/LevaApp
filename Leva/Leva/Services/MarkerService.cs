﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Plugin.Geolocator.Abstractions;
using Xamarin.Forms.Maps;

namespace Leva
    {
    class MarkerService
        {
        private static  MarkerService          s_instance;
        private List<LevaMarker>                   m_makerList;

        public MarkerService ()
            {
            m_makerList = new List<LevaMarker>();
            }

        public static MarkerService Instance
            {
            get
                {
                if ( s_instance == null )
                    s_instance = new MarkerService();

                return s_instance;
                }
            }

        public List<LevaMarker> MarkerList
            {
            get
                {
                return m_makerList;
                }
            set
                {
                m_makerList = value;
                }
            }

        public void UpdateAndRetrieveMarkerList()
            {
            //var pin1 = new Pin
            //    {
            //    Type = PinType.Place,
            //    Position = new Xamarin.Forms.Maps.Position( 46.780923, -71.274683 ),
            //    Label = "ULaval",
            //    Address = ""
            //    };         
            //Marker marker1 = new Marker (1, pin1, "ULaval", "ULaval@Ulaval.ca");
            //m_makerList.Add( marker1 );

            //var pin2 = new Pin
            //    {
            //    Type = PinType.Place,
            //    Position = new Xamarin.Forms.Maps.Position( 46.786653, -71.282151 ),
            //    Label = "Pyramide",
            //    Address = ""
            //    };
            //Marker marker2 = new Marker (2, pin2, "Pyramide", "Pyramide@hotmail.com");
            //m_makerList.Add( marker2 );
            }
        }
    }

