﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms.Maps;
using Xamarin.Forms;

namespace Leva
    {
	public partial class LevaMapView : ContentView
	    {
		public LevaMapView ()
		    {
			InitializeComponent ();
            CenterMapOnPostion();
            PutSampleDataOnMap();
            }

        private async void CenterMapOnPostion ()
            {
            Plugin.Geolocator.Abstractions.Position position;
            if ( LocationService.Instance.IsPositionAvailable() )
                position = LocationService.Instance.GetFastPosition;
            else
                position = await LocationService.Instance.GetLocationAsync();

            levaMap.MoveToRegion(
                MapSpan.FromCenterAndRadius(
                    new Position(position.Latitude, position.Longitude), Distance.FromKilometers(1)));
            }

        void PutSampleDataOnMap ()
            {
            Position            tourEiffel  = new Position(48.859217, 2.293914);
            LevaMarkerList     places      = LevaMarkerList.Instance;

            foreach ( LevaMarker p in places )
                {
                levaMap.AddCustomPin(p.CreateCustomPin(p.IsPink ? "pin.png" : "pin.png"));
                }

            levaMap.MoveToRegion(MapSpan.FromCenterAndRadius(tourEiffel, Distance.FromKilometers(2.5)));
            }
        }
    }
