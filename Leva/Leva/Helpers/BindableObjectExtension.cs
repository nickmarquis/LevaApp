﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq.Expressions;
using System.ComponentModel;

namespace Xamarin.Forms
    {
    public delegate void PropertyChangedHandler ( string propertyName );

    public static class BindableObjectExtensions
        {


        public static void OnPropertyChangedExt<TProperty> ( this BindableObject obj, Expression<Func<TProperty>> property, PropertyChangedHandler propertyChangedHandler )
            {
            if ( property == null || propertyChangedHandler == null )
                return;

            string      name = GetPropertyName(property);

            propertyChangedHandler.Invoke(name);
            }

        public static string GetPropertyName<TProperty> ( Expression<Func<TProperty>> property )
            {
            if ( property == null )
                return null;

            var expression = property.Body as MemberExpression;

            if ( expression == null || expression.Member == null )
                return null;

            return expression.Member.Name;
            }
        }

    public class TestBindableExtension : BindableObject
        {
        string      _test       = "Test";
        public string TestProperty
            {
            get { return _test; }
            set
                {
                _test = value;
                this.OnPropertyChangedExt(() => TestProperty, this.OnPropertyChanged);
                }
            }
        }
    }