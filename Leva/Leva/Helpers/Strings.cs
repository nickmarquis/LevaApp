﻿using System;

namespace Leva
{
	public static class Strings
	{
		public static readonly string About = "About";
		public static readonly string AboutLeva = "About Leva";
		
		public static readonly string CreatingAccount = "Creating Account";
		public static readonly string DeleteAccount = "Delete Account";
		public static readonly string Email = "Email";
		public static readonly string ErrorOcurred = "Error Occurred";
		public static readonly string InvalidCredentials = "Invalid Credentials";
		public static readonly string Leva = "Leva";
		public static readonly string MomentSent = "Fun Sent";
		public static readonly string NoInternetConnection = "No Internet Connection";
		public static readonly string Send = "Send";
		public static readonly string SendingMoment = "Sending Fun";
		public static readonly string SendMoment = "Send Fun";
		public static readonly string SignIn = "Sign In";
		public static readonly string SigningIn = "Signing In";
		public static readonly string SignOut = "Sign Out";
		public static readonly string SignUp = "Sign Up";
		public static readonly string TermsOfUse = "Terms of Use";

		public static readonly string ThreeSeconds = "Three Seconds";
		public static readonly string FourSeconds = "Four Seconds";
		public static readonly string FiveSeconds = "Five Seconds";
		public static readonly string SixSeconds = "Six Seconds";
		public static readonly string SevenSeconds = "Seven Seconds";
		public static readonly string EightSeconds = "Eight Seconds";
		public static readonly string NineSeconds = "Nine Seconds";
		public static readonly string TenSeconds = "Ten Seconds";

		public static readonly string MadeWithXamarinFormsLink = "https://xamarin.com/forms";
		public static readonly string MomentsGitHubLink = "https://github.com/pierceboggan/moments";
	
		public static readonly string TermsLink = "https://github.com/pierceboggan/moments/blob/master/Terms.html";
		public static readonly string PrivacyPolicyLink = "https://github.com/pierceboggan/moments/blob/master/PrivacyPolicyLink.html";
	}
}

