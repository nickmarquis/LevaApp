﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace Leva
{
	public partial class LeventPage : ContentPage
        {
		public LeventPage ( string leventId )
		    {
            BindingContext = new LeventViewModel( leventId );
            InitializeComponent ();

            SetupEventHandlers();
            }

        private LeventViewModel ViewModel
            {
            get { return BindingContext as LeventViewModel; }
            }

        private void SetupEventHandlers ()
            {

            }
        }
}
