﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace Leva
    {
    public partial class LoginPage : ContentPage
        {
        public LoginPage ()
            {
            InitializeComponent();

            NavigationPage.SetHasNavigationBar(this, false);

            App.OnFacebookAuthSuccess = () =>
            {
                //var cred = AmazonUtils.Credentials;
                AccountService.Instance.RetrieveFacebookProfile();
                NavigateToMainUI();
            };

            App.OnFacebookAuthFailed = () =>
            {

            };
            }

        private void NavigateToMainUI ()
            {
            App.Current.MainPage = App.FetchMainUI();
            }
        }
    }
