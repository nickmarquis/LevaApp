﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;

namespace Leva
    {
	public partial class SettingPage : BasePage
        {
		public SettingPage ()
		    {
            BindingContext = new SettingViewModel();

            InitializeComponent ();
            SetupEventHandlers();
            }

        private void SetupEventHandlers ()
            {
            logOutButton.Tapped += LogOutTapped;
            }

        /// <summary>
        /// Handles the Click event of the Button control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private void LogOutTapped ( object sender, EventArgs e )
            {
            AccountService.Instance.SignOut();
            App.Current.MainPage = new LoginPage();
            }
        }
    }
