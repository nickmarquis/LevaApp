﻿using System;
using System.Collections.Generic;

using Xamarin.Forms;

namespace Leva
    {
    public partial class ProfilePage : BasePage
        {
        public ProfilePage()
            {
            BindingContext = new ProfileViewModel();

            InitializeComponent();
            SetupEventHandlers();
            }

        private void SetupEventHandlers ()
            {
            settingButton.Clicked += SettingClick;
            }

        /// <summary>
        /// Handles the Click event of the Button control.
        /// </summary>
        /// <param name="sender">The source of the event.</param>
        /// <param name="e">The <see cref="EventArgs"/> instance containing the event data.</param>
        private async void SettingClick ( object sender, EventArgs e )
            {
            var settingPage = new SettingPage ();
            await Navigation.PushAsync( settingPage );
            }
        }
    }
