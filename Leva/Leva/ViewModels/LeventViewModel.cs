﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using Xamarin.Forms;

namespace Leva
{
    public class LeventViewModel : BaseViewModel
        {
        string                                       m_leventId;
        private ObservableCollection<LeventMedia>    m_LeventMedias;

        private Command fetchLeventMediasCommand;
        private Command destroyLeventMediasCommand;

        public LeventViewModel ( string leventId )
            {
            m_leventId = leventId;
            m_LeventMedias = new ObservableCollection<LeventMedia>();
            }

        public string LeventId
            {
            get { return m_leventId; }
            }

        public ObservableCollection<LeventMedia> LeventMedias
            {
            get
                {
                return m_LeventMedias;
                }
            set
                {
                m_LeventMedias = value;
                OnPropertyChanged( "LeventMedias" );
                }
            }

        public Command FetchLeventMediasCommand
            {
            get { return fetchLeventMediasCommand ?? ( fetchLeventMediasCommand = new Command( async () => await ExecuteFetchLeventMediasCommand() ) ); }
            }

        public Command DestroyLeventMediasCommand
            {
            get { return destroyLeventMediasCommand ?? ( destroyLeventMediasCommand = new Command( async ( object parameter ) => await ExecuteDestroyLeventMediasCommand( parameter ) ) ); }
            }

        public async Task ExecuteFetchLeventMediasCommand ()
            {
            if ( IsBusy )
                {
                return;
                }

            IsBusy = true;

            try
                {
                if ( await ConnectivityService.IsConnected() )
                    {
                    m_LeventMedias.Clear();
                    var refreshedLeventMedia = await MomentService.Instance.GetMoments ();
                    m_LeventMedias.AddRange( refreshedLeventMedia );
                    }
                else
                    {
                    DialogService.ShowError( Strings.NoInternetConnection );
                    }
                }
            catch ( Exception ex )
                {
                Xamarin.Insights.Report( ex );
                }

            IsBusy = false;
            }

        public async Task ExecuteDestroyLeventMediasCommand ( object parameter )
            {
            if ( IsBusy )
                {
                return;
                }

            IsBusy = true;

            try
                {
                var moment = parameter as LeventMedia;
                await DestroyLeventMedias( moment );
                }
            catch ( Exception ex )
                {
                Xamarin.Insights.Report( ex );
                }

            IsBusy = false;
            }

        private async Task DestroyLeventMedias ( LeventMedia leventMedia )
            {
            m_LeventMedias.Remove( leventMedia );

            await MomentService.Instance.DestroyMoment( leventMedia );
            }

        }
    }
