﻿using System;
using System.Net;
using Xamarin.Forms;
using Plugin.Settings;

namespace Leva
    {
    public class SettingViewModel : BaseViewModel
        {
        public string ProfileName
            {
            get { return CrossSettings.Current.GetValueOrDefault<string>( "profileName" ); }
            }

        public string ProfileEmail
            {
            get { return CrossSettings.Current.GetValueOrDefault<string>( "profileEmail" ); ; }
            }
        }
    }
