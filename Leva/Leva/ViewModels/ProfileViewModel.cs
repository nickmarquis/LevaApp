﻿using System;
using System.Net;
using Xamarin.Forms;
using Plugin.Settings;

namespace Leva
    {
    public class ProfileViewModel : BaseViewModel
        {
        public string ProfileName
            {
            get { return CrossSettings.Current.GetValueOrDefault<string>("profileName"); }
            }

        public string ProfileImageUrl
            {
            get { return CrossSettings.Current.GetValueOrDefault<string>("profileImage"); ; }
            }
        }
    }