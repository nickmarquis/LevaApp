﻿using System;
using System.ComponentModel;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Leva
    {
    public partial class App : Application
        {
        public static Action OnFacebookAuthSuccess;
        public static Action OnFacebookAuthFailed;
        public App ()
            {
            InitializeComponent();

            if ( AccountService.Instance.ReadyToSignIn )
                {
                MainPage = FetchMainUI();
                }
            else
                MainPage = new LoginPage();
            }

        public static NavigationPage FetchMainUI ()
            {
            var mapPage = new MapPage();
            var cameraPage = new CameraPage();
            var profilePage = new ProfilePage();

            var tabContainerPage = new TabContainerPage
                {
                Children = {
                    cameraPage,
                    mapPage,
                    profilePage
                },
                CurrentPage = mapPage
                };
            var navigationPage = new NavigationPage (tabContainerPage)
                {
                BarBackgroundColor = Colors.NavigationBarColor,
                BarTextColor = Colors.NavigationBarTextColor
                };

            if ( Device.OS != TargetPlatform.Android )
                {
                NavigationPage.SetHasNavigationBar(tabContainerPage, false);
                tabContainerPage.CurrentPage = mapPage;
                }
            else
                {
                tabContainerPage.Title = "Map";
                tabContainerPage.CurrentPage = mapPage;
                NavigationPage.SetHasNavigationBar( tabContainerPage, false );
                }

            tabContainerPage.PropertyChanged += ( object sender, PropertyChangedEventArgs e ) => {
                if ( e.PropertyName == "CurrentPage" )
                    {
                    var currentPageType = tabContainerPage.CurrentPage.GetType ();

                    if ( currentPageType == typeof(MapPage) )
                        {
                        NavigationPage.SetHasNavigationBar(tabContainerPage, false );
                        tabContainerPage.Title = "Map";
                        }
                    else if ( currentPageType == typeof(CameraPage) )
                        {
                        NavigationPage.SetHasNavigationBar(tabContainerPage, false);
                        tabContainerPage.Title = "Camera";
                        }
                    else if ( currentPageType == typeof(ProfilePage) )
                        {
                        NavigationPage.SetHasNavigationBar(tabContainerPage, false );
                        tabContainerPage.Title = "Profile";
                        }
                    }
            };

            tabContainerPage.CurrentPageChanged += ( ) => {
                var currentPage = tabContainerPage.CurrentPage as BasePage;
                if ( tabContainerPage.CurrentPage.GetType() == typeof(MapPage) && Device.OS == TargetPlatform.iOS )
                    { 
                    currentPage.LeftToolbarItems.Clear();
                    //currentPage.LeftToolbarItems.Add(new ToolbarItem
                    //    {
                    //    Icon = "FriendRequestsButton.png",
                    //    Command = new Command(() => currentPage.Navigation.PushModalAsync(new NavigationPage(new FriendRequestsPage())
                    //        {
                    //        BarBackgroundColor = Colors.NavigationBarColor,
                    //        BarTextColor = Colors.NavigationBarTextColor
                    //        }, true)),
                    //    Priority = 1
                    //    });
                    }
                else
                    {
                    currentPage.LeftToolbarItems.Clear();
                    }
            };

            return navigationPage;
            }

        private void LoadProfil()
            {
            AccountService.Instance.RetrieveFacebookProfile();
            }

        protected override void OnStart ()
            {
            // Handle when your app starts
            }

        protected override void OnSleep ()
            {
            // Handle when your app sleeps
            }

        protected override void OnResume ()
            {
            // Handle when your app resumes
            }
        }
    }