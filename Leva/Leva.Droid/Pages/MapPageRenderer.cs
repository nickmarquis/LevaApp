using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using Java.IO;
using Java.Net;
using System.IO;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Graphics;
using Android.Widget;
using Android.Views;
using Leva;
using Leva.Droid;
using Xamarin.Forms.Maps.Android;
using Android.Gms.Maps;
using Xamarin.Forms.Maps;
using Xamarin.Forms;
using Android.Gms.Maps.Model;
using Xamarin.Forms.Platform.Android;

[assembly: ExportRenderer( typeof( LevaMap ), typeof( MapPageRenderer ) )]
namespace Leva.Droid
    {
    public class MapPageRenderer : MapRenderer, GoogleMap.IInfoWindowAdapter, IOnMapReadyCallback
        {
        bool                m_isDrawDone;
        ImageView           m_pinImage = null;

        LevaMarkerView      m_levaMarkerView = new LevaMarkerView();
        GoogleMap           m_map;
        IList<Pin>          m_mapPins;
        TileOverlayOptions  m_tileOptions;

        protected override void OnElementChanged ( ElementChangedEventArgs<Xamarin.Forms.View> e )
            {
            base.OnElementChanged(e);

            if ( e.OldElement != null )
                {
                m_map.InfoWindowClick -= LevaMarkerViewClicked;
                }

            if ( e.NewElement != null )
                {
                MapView mapView = Control as MapView;
                var formsMap        = e.NewElement as LevaMap;

                var tileProvider = new CustomTileProvider (512, 512, formsMap.MapTileTemplate);
                m_tileOptions = new TileOverlayOptions().InvokeTileProvider(tileProvider);

                m_mapPins = formsMap.Pins;
                mapView.GetMapAsync(this);
                }
            }

        public void OnMapReady ( GoogleMap googleMap )
            {
            m_map = googleMap;

            m_map.InfoWindowClick += LevaMarkerViewClicked;

            // required if you wish to handle info window and its content
            m_map.SetInfoWindowAdapter(this);
            }


        protected override void OnElementPropertyChanged ( object sender, PropertyChangedEventArgs e )
            {
            base.OnElementPropertyChanged(sender, e);

            if ( m_isDrawDone )
                return;

            if ( e.PropertyName.Equals("VisibleRegion") )
                {
                m_map.Clear();
                var tileOverlay = m_map.AddTileOverlay( m_tileOptions );
                m_map.MarkerClick += HandleMarkerClick;
                m_map.InfoWindowClick += LevaMarkerViewClicked;

                foreach ( var pin in m_mapPins )
                    {
                    var         marker      = new MarkerOptions ();
                    LevaPin  customPin      = pin.BindingContext as LevaPin;

                    marker.SetPosition(new LatLng(pin.Position.Latitude, pin.Position.Longitude));
                    marker.SetTitle(pin.Label);

                    if ( !string.IsNullOrEmpty(pin.Address) )
                        marker.SetSnippet(pin.Address);
                    else
                        marker.SetSnippet("");

                    //marker.InfoWindowAnchor(10f, 5f);

                    BitmapDescriptor    bmp     = null;

                    if ( customPin != null && !string.IsNullOrEmpty(customPin.IconResource) )
                        {
                        string  resourceName    = System.IO.Path.GetFileNameWithoutExtension(customPin.IconResource);
                        int     resourceId      = Context.Resources.GetIdentifier(resourceName, "drawable", Context.PackageName);

                        try
                            {
                            bmp = BitmapDescriptorFactory.FromResource(resourceId);
                            }
                        catch ( Exception )
                            {
                            bmp = null;
                            }

                        if ( bmp == null )
                            {
                            try
                                {
                                bmp = BitmapDescriptorFactory.FromResource(Resource.Drawable.pin);
                                }
                            catch ( Exception )
                                {
                                bmp = null;
                                }
                            }
                        }

                    if ( bmp != null )
                        marker.SetIcon(bmp);
                    else
                        marker.SetIcon(BitmapDescriptorFactory.DefaultMarker());

                    m_map.AddMarker(marker);
                    }

                m_isDrawDone = true;
                }
            }



        private void LevaMarkerViewClicked ( object sender, GoogleMap.InfoWindowClickEventArgs e )
            {
            var             marker  = e == null ? null : e.Marker;
            LevaMap         myMap   = this.Element as LevaMap;

            if ( myMap == null || marker == null )
                return;

            LevaPin  customPin   = myMap.GetPinAtPosition(new Position (marker.Position.Latitude, marker.Position.Longitude));

            if ( customPin != null )
                customPin.RaiseClickEvent();
            }

        async void HandleMarkerClick ( object sender, GoogleMap.MarkerClickEventArgs e )
            {
            var             marker  = e == null ? null : e.Marker;      //.P0;

            if ( marker == null || marker.IsInfoWindowShown )
                return;

            LevaMap              myMap   = this.Element as LevaMap;

            if ( myMap == null )
                return;

            Position    currentPos      = new Position (marker.Position.Latitude, marker.Position.Longitude);
            Distance    currentRadius   = myMap.VisibleRegion.Radius;

            // center the map on the clicked pushpin
            myMap.MoveToRegion( MapSpan.FromCenterAndRadius( currentPos, currentRadius ) );

            //marker.ShowInfoWindow();

            LevaPin customPin = myMap.GetPinAtPosition(new Position(marker.Position.Latitude, marker.Position.Longitude));
            await ( (LevaMap)Element ).StartLevantClick(customPin);

            }

        protected override void OnLayout ( bool changed, int l, int t, int r, int b )
            {
            base.OnLayout(changed, l, t, r, b);

            //NOTIFY CHANGE
            if ( changed )
                {
                m_isDrawDone = false;
                }
            }

        public Android.Views.View GetInfoContents ( Marker marker )
            {
            // note: return null if you used the GetInfoWindow to return the entire info window

            LevaMap myMap = this.Element as LevaMap;

            if ( myMap == null || marker == null )
                return null;

            LevaPin customPin = myMap.GetPinAtPosition(new Position(marker.Position.Latitude, marker.Position.Longitude));

            if ( customPin == null )
                return null;

            double          density         = Resources.DisplayMetrics.Density;
            double          infoWidth       = this.Control.Width * 0.90,
                            infoHeight      = this.Control.Height * 0.90;
            LevaMarker      place           = customPin.DataObject as LevaMarker;
            int             nTextLines      = 1;
            int             wrapLength      = (int)(infoWidth / density / 6.0); // estimated max chars before text wraps

            // count lines in the place's info text

            if ( place != null && !string.IsNullOrEmpty(place.MapPinText) )
                {
                var             linesArray  = place.MapPinText.Split(new char[] { '\n'});
                List<string>    lines       = linesArray == null ? null : linesArray.ToList(); ;

                if ( lines != null && lines.Count > 1 )
                    {
                    nTextLines = lines.Count;

                    foreach ( string s in lines )
                        {
                        // empiric: lines are mostly wrapped at 38 with our dimesnions
                        if ( s.Length >= wrapLength )
                            nTextLines++;
                        }
                    }
                else if ( place.MapPinText.Length >= wrapLength )
                    nTextLines++;

                //nTextLines	= place.MapPinText.Count(c => c == '\n');
                }

            infoHeight += nTextLines * 12 * density;

            m_levaMarkerView.BindingContext = place;

            ViewGroup   viewGrp     = DroidHelper.ConvertFormsToNative(m_levaMarkerView.Content, new Rectangle(0, 0, infoWidth, infoHeight));

            // transform the native control into a bitmap
            Bitmap      bmp         = DroidHelper.ViewGroupToBitmap(viewGrp, this.Context, (int)infoWidth, (int)infoHeight, density, true);

            if ( m_pinImage == null )
                m_pinImage = new ImageView(this.Context);

            m_pinImage.SetImageBitmap(bmp);

            return m_pinImage;
            }


        public Android.Views.View GetInfoWindow ( Marker marker )
            {
            // return the entire info window or:
            // return null for the GetInfoContents method to be used
            return null;
            }

        }
    }