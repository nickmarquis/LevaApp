using Xamarin.Forms;
using Android.App;
using System;
using Xamarin.Forms.Platform.Android;
using Leva;
using Leva.Droid;
using Xamarin.Facebook.Login;
using Xamarin.Facebook;
using System.Collections.Generic;
using Object = Java.Lang.Object;
using View = Android.Views.View;

[assembly: ExportRenderer(typeof(FacebookLoginButton), typeof(FacebookLoginButtonRenderer))]
namespace Leva.Droid
    {
    public class FacebookLoginButtonRenderer : ButtonRenderer
        {
        private static Activity activity;

        protected override void OnElementChanged ( ElementChangedEventArgs<Button> e )
            {
            base.OnElementChanged(e);

            activity = this.Context as Activity;

            if ( this.Control != null )
                {
                Android.Widget.Button button = this.Control;
                button.SetOnClickListener(ButtonClickListener.Instance.Value);
                }
            }

        private class ButtonClickListener : Object, IOnClickListener
            {
            public static readonly Lazy<ButtonClickListener> Instance = new Lazy<ButtonClickListener>(() => new ButtonClickListener());

            public void OnClick ( View v )
                {
                LoginManager.Instance.LogInWithReadPermissions(activity, new List<string> { "public_profile", "user_friends", "email" });
                }
            }

        }
    }
