using System;
using System.Threading.Tasks;
using Android.Hardware;
using Android.OS;
using Android.Views;
using Android.Content;
using Android.Runtime;
using Android.App;
using Xamarin.Forms;
using Leva;
using Newtonsoft.Json;
using System.Net;
using Xamarin.Facebook;
using Xamarin.Facebook.Login;

[assembly: Xamarin.Forms.Dependency(typeof(Leva.Droid.AccountServiceImplementation))]
namespace Leva.Droid
    {

    public class AccountServiceImplementation : Java.Lang.Object, IAccountService, GraphRequest.IGraphJSONObjectCallback 
        {
        public AccountServiceImplementation () { }

        public void RetrieveFacebookProfile ()
            {
            GraphRequest request = GraphRequest.NewMeRequest(AccessToken.CurrentAccessToken, this);

            Bundle parameters = new Bundle();
            parameters.PutString("fields", "id,name,email");
            request.Parameters = parameters;
            request.ExecuteAsync();
            }

        public void OnCompleted ( Org.Json.JSONObject json, GraphResponse response )
            {
            if ( json == null )
                App.OnFacebookAuthFailed();
            else
                {
                string data = json.ToString();
                Profile result = JsonConvert.DeserializeObject<Profile>(data);
                result.UserToken = AccessToken.CurrentAccessToken.Token;
                result.ProfilePicturePath = new Uri("http://graph.facebook.com/" + result.Id + "/picture?type=large");
                AccountService.Instance.SaveUser(result);
                }
            }

        }
    }