using Android.Gms.Maps.Model;
using System;

namespace Leva.Droid
    {
    public class CustomTileProvider : UrlTileProvider
        {
        string urlTemplate;

        private static double[] TILES_ORIGIN = {-20037508.34789244, 20037508.34789244};//TODO Duplicate from WMS PROVIDER, put as utils
                                                                                             // Size of square world map in meters, using WebMerc projection.
        private static double MAP_SIZE = 20037508.34789244 * 2;//TODO Duplicate from WMS PROVIDER, put as utils
        private static double ORIGIN_SHIFT = Math.PI * 6378137d;

        public CustomTileProvider ( int x, int y, string urlTemplate )
            : base( x, y )
            {
            this.urlTemplate = urlTemplate;
            }

        private double tile2long ( int x, int z)
            {
            return ( x / Math.Pow( 2, z ) * 360 - 180 );
            }

        private double tile2lat ( int y, int z)
            {
            var n=Math.PI-2*Math.PI*y/Math.Pow(2,z);
            return ( 180 / Math.PI * Math.Atan( 0.5 * ( Math.Exp( n ) - Math.Exp( -n ) ) ) );
            }


        public override Java.Net.URL GetTileUrl ( int x, int y, int z )
            {
            //string newX = tile2long(x, z).ToString();
            //string newY = tile2lat(y, z).ToString();
            //string XTemp = newX.Replace( ",", "." );
            //string YTemp = newY.Replace( ",", "." );

            var url = urlTemplate.Replace("{z}", z.ToString()).Replace("{x}", x.ToString()).Replace("{y}", y.ToString());
            Console.WriteLine( url );
            return new Java.Net.URL(url );
            }
        }
    }