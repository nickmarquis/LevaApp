﻿using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

using Xamarin.Forms;
using Xamarin.Forms.Platform.Android;
using ImageCircle.Forms.Plugin.Droid;
using System.Threading.Tasks;
using System;
using Xamarin.Facebook;
using Xamarin.Facebook.Login;
using Acr.UserDialogs;
using Leva;

namespace Leva.Droid
    {
    [Activity(Label = "Leva", Icon = "@drawable/icon", ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation, ScreenOrientation = ScreenOrientation.Portrait)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsApplicationActivity
        {

        ICallbackManager                         callbackManager;

        protected override void OnCreate ( Bundle bundle )
            {
            base.OnCreate(bundle);

            ActionBar.Hide();

            global::Xamarin.Forms.Forms.Init(this, bundle);
            global::Xamarin.FormsMaps.Init(this, bundle);
            UserDialogs.Init(() => (Activity)Forms.Context);

            LoadApplication(new App());
            SetupThirdPartyLibraries();
            SetupFacebookManager();

            LoadProfil();
            }

        private void SetupThirdPartyLibraries ()
            {
            Xamarin.Insights.Initialize(Leva.Keys.InsightsKey, this);
            }

        private void SetupFacebookManager ()
            {
            FacebookSdk.SdkInitialize(this.ApplicationContext);
            callbackManager = CallbackManagerFactory.Create();

            LoginManager.Instance.RegisterCallback(callbackManager, new FacebookCallback<Xamarin.Facebook.Login.LoginResult>()
                {
                HandleSuccess = loginResult =>
                {
                    if(!String.IsNullOrEmpty(loginResult.AccessToken.Token))
                        App.OnFacebookAuthSuccess();
                    else
                        App.OnFacebookAuthFailed();
                },
                HandleCancel = () =>
                {
                    App.OnFacebookAuthFailed();
                },
                HandleError = loginError =>
                {
                    App.OnFacebookAuthFailed();
                }
                });
            }

        private void LoadProfil ()
            {
            if ( Leva.AccountService.Instance.UserProfil == null )
                Leva.AccountService.Instance.RetrieveFacebookProfile();
            }

        protected override void OnActivityResult ( int requestCode, Result resultCode, Android.Content.Intent data )
            {
            base.OnActivityResult(requestCode, resultCode, data);
            callbackManager.OnActivityResult(requestCode, (int)resultCode, data);
            }


        internal class FacebookCallback<TResult> : Java.Lang.Object, IFacebookCallback where TResult : Java.Lang.Object
            {
            public Action HandleCancel { get; set; }
            public Action<FacebookException> HandleError { get; set; }
            public Action<TResult> HandleSuccess { get; set; }

            public void OnCancel ()
                {
                var c = HandleCancel;
                if ( c != null )
                    c();
                }

            public void OnError ( FacebookException error )
                {
                var c = HandleError;
                if ( c != null )
                    c(error);
                }

            public void OnSuccess ( Java.Lang.Object result )
                {
                var c = HandleSuccess;
                if ( c != null )
                    c(result.JavaCast<TResult>());
                }

            }
        }
    }

